USE ModernWays;
CREATE TABLE Boeken(
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Categorie VARCHAR(100) CHAR SET utf8mb4 NOT NULL);
INSERT INTO Boeken(Voornaam,Familienaam,Titel,Categorie)
VALUES
('Stephen','Hawking','The Nature of Space and Time','Wiskunde'),
('Stephen','Hawking','Antwoorden op de grote vragen','Filosofie'),
('William','Dunham','Journey through Genius: The Great Theorems of Mathematics','Wiskunde'),
('William','Dunham','Euler: The Master of Us All','Geschiedenis'),
('Evert Willem','Beth','Mathematical Thought','Filosofie');