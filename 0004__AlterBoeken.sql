-- herstel de kolom
-- deze mag tot 150 (mogelijk internationale) karakters bevatten
USE ModernWays;
ALTER TABLE Boeken ADD COLUMN Commentaar VARCHAR(150) CHARSET utf8mb4;